import animal.Animal;

import java.util.Scanner;

public class ZooController {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please insert animal name: ");
        String animalName = keyboard.nextLine();
        String species = keyboard.nextLine();
        int age = keyboard.nextInt();

        Animal firstAnimal = new Animal(animalName, species, age);
        System.out.println("SavedAnimal: "+ firstAnimal.getName() + firstAnimal.getSpecies() + firstAnimal.getAge());

        firstAnimal.setName("George");
        System.out.println("Saved Animal: "+ firstAnimal.getName() + firstAnimal.getSpecies() + firstAnimal.getAge());
        firstAnimal.setSpecies("Caine");
        System.out.println("Saved Animal: "+ firstAnimal.getName() + firstAnimal.getSpecies() + firstAnimal.getAge());
        firstAnimal.setAge(9);
        System.out.println("Saved Animal: "+ firstAnimal.getName() + firstAnimal.getSpecies() + firstAnimal.getAge());
    }

}
