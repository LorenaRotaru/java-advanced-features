package animal;

public class Animal {

    //private, protected, public
    //default is protected
    private String name;
    private String species = "";
    private int age;

    public Animal(String nameParameter, String species, int age) {
        this.name = nameParameter;
        this.species = species;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSpecies() {
        return species;
    }
    public int getAge() {
        return age;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public void setSpecies(String newSpecies){
        this.species = newSpecies;
    }

    public void setAge(int newAge){
        this.age = newAge;
    }
}




//       System.out.println("Species as param " + species);
//       System.out.println("Species as attribute " + this.species);
//       this.species = species;
//       System.out.println("Species as attribute after it is set " + this.species);
//    }

   // public void setName() {
//        String name = "lalala";
//        this.name = "mine";
  //  }

